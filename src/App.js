import React from 'react';
import './App.css';
import tea from './tea.jpg'

function App() {
  const date = new Date();
  const isTime = date.getHours() === 15 
  return (
    <div className="background">
      <h1 className="comic blue">Is it Tea time yet ?</h1>
          <h2 className="comic green">{
            isTime ? "Yes" : "No"
          }</h2>
        <img src={tea} alt="tea time"/>
    </div>
  );
}

export default App;
